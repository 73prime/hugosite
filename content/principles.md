---
title: "Principles & Outcomes"
date: 2020-09-28T12:13:56-05:00
draft: false
---

We believe that principles should drive what we do. These in turn, lead to outcomes.

## Principles

Principles are defined as tactical philosophies by which we govern our decision-making and work product.

* Automation is better than Manual
* Renting is better than Buying
* Learning is better than "No Downtime"
* Small Deployments are better than Large Deployments
* Developer Empowerment is better than "Hero mindset"
* Measurable Success is better than Hope

## Outcomes

It's by leveraging the above principles that we can achieve the following outcomes:

* Security built in
* Faster development
* Better TCO
* Traceability built in
* Fail fast/fail early