---
title: "Gitlab, FTW"
date: 2022-01-22T13:00:15-05:00
showDate: true
draft: false
tags: ["blog","reflection"]
---

![Gitlab](/gitlab.png)

Simple is almost always better...

I've transitioned things from AWS Amplify to Gitlab pages. I've learned over the years to just value simplicity. Why introduce extra services?

Gitlab has become very robust over the years, and with the ability to host static sites via Gitlab Pages, well, it's a no-brainer to simply leverage the out-of-the-box capabilities.

