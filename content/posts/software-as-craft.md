---
title: "Software as Craft"
date: 2020-03-28T12:29:12-05:00
showDate: true
draft: false
tags: ["blog","story"]
---
![Craft](/pottery.jpg)

For some reason, I just encountered this open letter from Slack to Microsoft, written in 2016 in the New York Times.

Now in 2020, sitting in the midst of the largest pandemic since 1918’s Spanish Flu, I’m wondering if the same letter could be written today (a little over 3 years later).

Both Teams and Slack have gained tremendous marketshare in the Enterprise, specifically due to new work-from-home policies that companies are implementing during quarantine.

As a user of both systems, I can say that both are packed with features for sure. However, there’s something in that open letter from several years ago that resonates with me:

>Building a product that allows for significant improvements in how people communicate requires a degree of thoughtfulness and craftsmanship that is not common in the development of enterprise software. How far you go in helping companies truly transform to take advantage of this shift in working is even more important than the individual software features you are duplicating.


Two words resonate out of the above paragraph:

* craftsmanship
* duplicating

Being a software engineer from training and employment, (and also hobbiest), the word “Craftsman” is something that I can really get behind. It’s a word that makes mind immediately go to the old bricklayers, potters, artisans, sculptors, and painters. It’s not a word that makes me think of bits and bytes. However, it’s a word I really like, when applied to bits and bytes. Craft. Craftsmanship. Art. Artisan.

These are words that I would not at all apply to all software. I wouldn’t apply it to today’s Enterprise software at all. However, I’ve met software craftsmen that I aspire to become. People like Martin Fowler, Uncle Bob Martin, Barbara Liskov….these older folks that went before me and paved the way to make software like a craft.

I think there can be beauty in software. It’s a creation from the mind of an artisan.

That second word, “duplicating” is not at all craftsmanship. It’s pragmatic, logical, and clinical. It’s analytical, and cold. It certainly has its place in software engineering: shell sorts, bubble-sorts, and all sorts of algorithms are copy/pasted every day in every system, to speed along the development of software. However, it’s something that is not craftsmanship, or art.

I think Slack is on to something with their description of the two products. I’m hoping that Teams can be described with this craftsman type adjective at some point in the future (or even now).

I would love for my software to be considered art. I know it’s not always…..especially when I’m pressed for time and am not putting my artist’s hat on. Occasionally though, I am super pleased with the outcome, and marvel at a certain loop, or a certain performance refactoring.

Art. Engineering. These are words that are not always mutually exclusive. In fact, when good software engineering practices produce art, it's now something that can be a joy for users to use. That's the sweet spot of software. That's where I want to be. That is "craft".

-ap