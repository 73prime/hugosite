---
title: "Api Starter"
date: 2020-10-01T16:23:45-05:00
showDate: true
draft: false
tags: ["project","go"]
---

I confess. I love slick things. One of these is the fabulous [Spring Initializr website](https://start.spring.io/).  I wondered: Can I do something similar?  The answer is yes, in a very simple, rudimentary way. 

I see the need to provide developers with scaffolded apps, very quickly. This is taking a HelloWorld concept, and ramping it up slightly. I wanted to include:
* A web framework to provide basic API capabilities (GET/POST)
* A basic testing framework structured
* A Dockerfile 
* A Makefile

So - I built this in Go (even the UI via Go Templates), but the first "boilerplate" API is Python3 with Flask as the web framework.  You can see the [repo here](https://gitlab.com/73prime/apistarter).  A working demo is [here](http://api.73prime.io/ui). 

What the Go environment executes? A [simple Bash script which blows out a directory structure, complete with tests](https://gitlab.com/73prime/apistarter/-/blob/master/scripts/bootstrap_python.sh).  It then will zip that up, and push that response back to the client.

Rube Goldberg? Yes. Enormously fun to build? Also yes.

-ap