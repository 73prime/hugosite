---
title: "Maker Guild"
date: 2022-04-29T06:46:20-06:00
showDate: true
draft: false
tags: ["blog","makerguild"]
---

TL;DR: [Maker Guild](https://makerguild.org) was created to foster curiosity-driven technical outcomes. Similar to a hackathon, but different. [Contact me](mailto:allen@73prime.io) to have your org added as a chapter to Maker Guild.

# The longer version 
A coworker and I in our roles as Innovators decided to grass-roots a guild of makers.  Part of what we think we should be about is to evangelize curiosity-driven outcomes.  

To this end, we held our first-ever #MakerGuild. I've created a [rather empty portal](https://makerguild.org) for this as a stub for future build-out. This idea is bigger than one company or one consultancy. 

The name "Maker Guild" comes from the following:
* Maker: One who makes.
* Guild: an association of persons of the same trade or pursuits, formed to protect mutual interests and maintain standards; a similar association, as of merchants or artisans.

In the spirit of innovation, this is our feeble attempt to foster a grass-roots curiosity-driven approach to kicking technology tires.

* What: Consortium of technology “makers” to collaborate on interesting concepts / technologies.

* Where: We meet in our slack space in a channel called #maker_guild.

* Who: Our Innovation Team to facilitate, but those within the company, specifically technology and related teams are welcome.

* When: Notionally, ~2 months, a 2-3 hour “maker time”.

* Why: Promote cross-team collaboration, curiosity-driven outcomes.

* How: Slack Huddles, Timeboxing, etc.

## Patterns / Rules
First, there aren’t many; however, in the spirit of attempting to make this exercise useful:

* Attempt to work in at least pairs.
* Attempt experiments that are achievably finished (or “landed”) in a 3 hour “Maker Time”. This may mean that you work after hours to complete it, or surface it into the next Maker Time if interested.
* Have show-caseable findings. This may be that it was an experiment that failed. Note: this isn’t a bad thing.
* Ensure you have your manager’s blessing to work on these things.
* Think of the SME’s you might need; we encourage you to think of SME’s in the org that you may not know, and you can evangelize to work with you.  

## Anti-patterns (i.e. What we should avoid)

“Boil the Ocean” scope. Seek to at least decompose the ideas into workable chunks. Much of the work might be unknown, (and this is OK); but strive for work that can “be shipped”.

## Early lessons from the Inaugural #MakerGuild

* Having "starter code" to bootstrap is necessary. In both teams, we leveraged existing recipes to get started.
* Be ready to call "audible". Adjust outcomes and plans.  Quicker is better than perfect.
* Decomposition of ideas is necessary. Make it small and achievable.
* Notional solutioning is required. Understand how you might solve the idea ahead of time, and "rough in" the solution.


Are you interested? [Drop me a line.](mailto:allen@73prime.io)