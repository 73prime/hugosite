---
title: "What if I'm Wrong"
date: 2020-04-26T12:23:11-05:00
showDate: true
draft: false
tags: ["blog","story"]
---
![falldown](/falldown.jpg)

I was pontificating and prophesying several months back while I was in a particularly foul mood. I was telling a man I respect all the things that would occur in a few months’ time, due to some pretty terrible decisions that were happening.

He let me finish, then asked a question which sort of startled me: “What if you’re wrong?”

That question had me pondering this for several days afterwards.

Fast forward a few months, and although I was actually correct in my foretelling of what the ramifications would be, that question still has a lasting effect on me.

I’m reading Mark Manson’s book “The Subtle Art of Not Giving a ______” at the moment, and the author has posited very similar question. The book has reminded me of the question that was first posed to me many months ago.

The book describes how failure and “wrongness” is actually not just a path to growth, but the path to growth. As a child constantly falls when she learns to walk, she’ll never rationalize the failure and give up, “Oh - I guess this walking thing isn’t for me.” The constant mistakes and blunders and willingness of “wrongness” is her path to growth.

It’s a lesson in humility and death to self and ego. If my identity is wrapped up into my “rightness”, then I will always be let down, and won’t be able to learn and grow, and in short, live.

There is wisdom in the thought of “death to self”. It’s only through death of ego and false identities can one truly live. I remember a quote from a book long ago that instructs us to be “a living sacrifice for others”. It’s a reminder that there is a true paradox to life….that to truly live, one must constantly grow; and the path to growth means death to self.

-ap