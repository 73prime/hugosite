---
title: "New Site"
date: 2020-09-27T07:49:43-05:00
showDate: true
draft: false
tags: ["blog","story"]
---

# Hugo,ftw

My previous site was a hand-rolled ReactJS site, for the sole purpose of learning ReactJS technologies, among other things. All the styling was my own, the colors, imaging, etc. 

I've decided that I should just embrace the fact that I really kind of suck at Front-end work. I don't consider the exercise a waste, as it highlighted what I _can_ be somewhat proficient at if I tried.

![the old site](/oldsite.png)

One of the key things that I really liked about the ReactJS site that I built is that I hand-rolled some blogging software based on a Firebase backend. The site would pull the content from a Firebase db, and render the pages accordingly.

![old site blog](/oldsiteblog.png)

I am moving back to a core philosophy of mine, which is to value simplicity.  To this end, this site is built atop the fabulous Hugo JAMStack, and deployed via Amplify to AWS Cloudfront. The workflow is:

```
build md files > commit/push > pipeline publishes to CDN
```

Can't get any easier.

-ap