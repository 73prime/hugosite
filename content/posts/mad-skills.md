---
title: "Mad Skills"
date: 2020-12-18T09:10:15-06:00
showDate: true
draft: false
tags: ["blog","story"]
---

![Napolean](/napolean.jpg)

I was reflecting today on what banal and un-appreciated skills that I'm overall appreciative of, as I watched my teenage daughter typing on the computer.

* Typing: 

I'm so thankful that I took Mrs. Hedges' typing class in 9th grade. Those old IBM Selectric's were loud, smelly, and not fun at all. But knowing home-keys, and being able to type without taking my eyes off the screen is so worth it in the long run. The ability to type unhindered by my eyes watching the keyboard has been highly beneficial in my career.

* Make coffee without electricity: 

Power outages, camping, and hiking don't hold me back. I can make a great cup of coffee with very little. In fact, a stove-top kettle with pour-over (or moka-pot) makes the best cup.

* Makefile usage:

The "make" build system is kind of old (first showing up in 1976). I hate to admit, that like many Microsoft-centric developers, I really didn't have a handle on this. In the last few years, every bit of my development activities have *not* been on the Microsoft environment. As such, I've learned the power of Makefiles. This is very underrated, but is highly valuable.

When you see a Makefile, you can understand how a certain package is _built_. Rather than simply following instructions in a Readme, one can see it perform. An example of a recent Makefile that I've written is [here](https://github.com/ahplummer/GoTemplateExample/blob/master/Makefile).
