---
title: "Analysis Paralysis"
date: 2021-04-07T06:10:15-05:00
showDate: true
draft: false
tags: ["blog","story","reflection"]
---

![Analysis Paralysis](/analysisparalysis.png)

One of my most favorite things to do is research.

* What Cloud provider provides the best Services?
* What is the best Domain Name Registrar?
* Where's the best VRBO / AirBnb in Pensacola?
* Which electric scooter is most cost effective?
* What headphones are the best and most economical?

These are just some of the things I've researched in the recent past. With each topic, there are nuanced answers, and more questions to research, reviews to read, charts to build.

It occurs to me that this can come at several costs:

* Opportunity Cost: While dickering over the finer points of battery wattage, I might just miss that last scooter in stock.  More expensively, in a company, I might miss out on the _opportunity of innovation_.  
  
* Team demotivation: As leaders struggle with decisions, for the "regular" folks, this can be demoralizing, as it is generally interpreted as indecision and weakness of their leadership.

* Expensive: In many ways delaying a decision will cost more in the long run.

    
## Prototype It

I've never heard of this, but defining it here, because it is true none-the-less:

> Opportunity of Innovation: There are those small chunks of time where teams can do innovation work without making final technology or process decisions. These are cheap "in and out" types of work that can evolve into something truly magical at best, and at worst, cost next to nothing to do.

One answer to analysis paralysis is to implement "tiger teams" with specific "Must-Have / Should-Have" requirements to do cheap "in and out" work to validate assumptions, or invalidate selections.

## Find Your Exit

In almost everything I buy these days (house, car, etc), I'm looking to the exit strategy: Will remodeling my kitchen bring me a high percentage ROI upon selling? Does this particular model car hold its value on resale?  

In technology, with its rapid pace, this is most important:
* Will the application architecture that we are designing lend itself to portability from Cloud X to Cloud Y?
* What length of term should I license this software for?
* Am I leveraging Open standards and practices, or locking into a walled garden?

![Thinker](/thinker.png)

Think through this rubric to see how it may help in your next decision:

1. Does this decision affect others?  There are hidden aspects to this in leadership.
2. Is this a "cheap" decision? Can you back-out with an exit strategy?
3. What things can be done to vet and rule out certain options?  Too many options adds to turmoil.

## Seek Help

Seek trusted help in your decisions. Added context is everything. Wrong technology decisions can be more expensive than none at all; but no decision can affect others much worse than you realize.