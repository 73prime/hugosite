---
title: "Devops on Rails"
date: 2020-03-01T12:27:48-05:00
showDate: true
draft: false
tags: ["blog","story"]
---
![Devops on rails](/rails.jpg)
DevOps “on rails” is a term that simply means “automate the automation”.

Generally speaking, a CI/CD tool is leveraged that has API capabilities to build and maintain pipelines. Tooling is then created to parse developers’ project declarations, using “enterprise plans” pre-configured with preset stages.

These dev projects will then get their CI/CD pipeline built automagically upon commit. The benefits are several:

* Prescribed, deterministic way of code delivery to a platform.
* Easy maintenance of pipelines for Day 2 new services in a platform.
* Developer speed-to-market.

-ap