---
title: "Process"
date: 2020-09-28T12:21:18-05:00
draft: false
---

We are very nimble with our engagements, but find if we can outline some form of process early on, it makes it easier for clients to understand how we operate.  These are general things; but can be used as a guideline as to how we operate.

1. Initial Consultation: This is usually free to you, and allows us to mutually assess our relationship, to see if we are a fit for you and your needs.

2. Inception: These are generally workshop(s) designed to elicit requirements, and develop a mutually agreeable workable backlog.

3. Norming: This is a session for a few hours that reviews the "way" that we will be working, whether we will be pairing with your team, or performing deskchecks, etc.

4. Sprints: Depending on the scope and scale of your work, we like to develop a time-bound model organizing work in small chunks. This generally makes the most sense for success of your project, as you will be able to see achievable progress.

5. The Daily Standup: We like to have product owner involvement as frequently as possible, but minimally, a technical leader should be available for a short 'standup'. This is where we typically go over:
   
* What did we work on?
* What will we be working on?
* Are there any blockers?
Note: It is called a 'standup' so that participants don't 'get comfortable' in a prolonged meeting. It encourages brevity.

6. The Showcase: At the culmination of each sprint, a brief 'showcase' is scheduled to show off work completed.

7. Day 2 Ops: If we are engaged with Day 2 operations, we may elect to move to a Kanban model.


