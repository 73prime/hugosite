---
title: "About"
date: 2020-09-27T07:44:54-05:00
draft: false
---

# Who is 73′?

73′ was formed in 2020 as an Oklahoma registered LLC.

* Some [Key Principles](/principles) can familiarize you with 
our core philosophies.
* The [73′ Process](/process) can familiarize you with the way we'll run engagements.

Many consultancies and contracting companies do fantastic technical
work on a per-project basis. However, there exists a gap with
respect to connecting technical deliverables to real, measurable
business outcomes. You'll soon discover when working with 73′ that
we will be asking "why" a lot. We believe that only by identifying
key performance indicators and measurable outcomes, can a project
really be deemed a success. Some key questions to ask yourself about
your technical needs:

* Have you determined the "how" a thing is to be done before the
"why" it should be done?

* What does "Day 2" operations look like after you've gotten your
technical deliverable?

* What is containerization of applications? 12-Factor apps? Are
monolith applications always bad?

* What is code quality? How can it be measured, and how is this
important to the success of my project?

Call us today. Let's chat about your needs. Maybe there's something
that we can help with; perhaps not. Regardless, you owe it to
yourself and your company to discover how we can help translate
technical things into business outcome.
