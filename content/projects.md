---
title: "Projects"
date: 2020-10-01T16:23:59-05:00
draft: false
---

# Projects that I work on...

[Splunk-CLI](https://gitlab.com/73prime/splunkcli): This is a program, written in the fabulous Go language that abstracts the Splunk REST API to facilitate doing things like:

  * Dashboard creation / deletion
  * Alert creation / deletion
  * Menu adjustments

The purpose of this CLI is to help facilitate CI/CD processes. In other words, you put this CLI into a Runner/Agent to scan a Git repo with the Splunk XML/YAML files that represent dashboards/alerts, and it will provision your Splunk instance. I've partnered with the fabulous [Day2On Cohort](https://day2on.org/posts/splunkcli/) to co-release and sponsor this work.

[Vault SSH One-Time-Password PoC](https://github.com/ahplummer/Vault-OTP-With-SSH-POC): This is a recipe that stands up two EC2's in AWS: one having Vault installed, and one playing the part of a 'regular instance'. This example shows how Vault can be used to generate and use One-Time passwords to gain access to a fleet of EC2's via SSH. This is fundamentally different than passing around private SSH keys.  

[og_platform_eng Docker Image](https://hub.docker.com/r/ahplummer/og_platform_eng) / [Github](https://github.com/ahplummer/og_platform_eng): This is a docker image that contains cloud and dev tools that I use quite often. Simply do a `docker pull ahplummer/og_platform_eng` to try things out. 

[AWS-SSHSecrets-POC](https://github.com/ahplummer/AWS-SSHSecrets-POC) This leverages SecretsManager in AWS to store a private SSH key to be able to log into a pre-provisioned EC2. This repo has the infra for EC2, key generation, and usage of the login script. This works by leveraging the "tagging" aspect of EC2's.

[Terraform LAMP Stack AWS Recipe](https://github.com/ahplummer/TerraformAWSLampStackRecipe) is a simple EC2 cluster with Apache, PHP installed, alongside MySQL RDS instances that are multi-AZ.
  
[API Starter](http://api.73prime.io/ui) is something I built after I discovered that I absolutely love the [Spring Initializer](https://start.spring.io/) project. This is my attempt to create a "boilerplate" for other API projects. My first one is Python3/Flask. I plan on adding other things into this (Golang, for example).  Things used:
  * Runs on AWS Lightsail
  * Is built with Golang (both UI and backend)
  * For now, simply executes a bash shell that spins out a project structure, then zips up contents and sends back to caller via a GET.
  * Code [repo here](https://gitlab.com/73prime/apistarter).

[Terraform Lightsail](https://github.com/ahplummer/terraform_lightsail) is my simple Terraform / Ansible scripting of AWS Lightsail. It's a good starter for someone.

[Terraform Azure Win10](https://github.com/ahplummer/recipe-Azure-Win10VM) is my Terraform/Packer scripts for standing up a base Window 10 environment in Azure. It's also a good starter for someone.

[CC-Trainer](https://github.com/ahplummer/cc-trainer) is written in Python, and works with Exchange that's _not_ MFA'd. This was birthed from my desire to train email senders to _not_ send me carbon-copy's if they are wanting action from me.

[AWSDocker](https://github.com/ahplummer/awsdocker) is an older project that encapsulates the AWS CLI along with the Cloud Foundry CLI. It's an environment for platform engineering shenanigans. It's a good starting point for shenanigans related to AWS and Cloud Foundry.

[EnvConsul-Vault](https://github.com/ahplummer/envconsul-vault) is my starting point of working with envconsul and Vault.

[Jutestring](https://github.com/ahplummer/jutestring) is my URL shortener service that I built for our Slack environment, and host on Lightsail. Feel free to poach.  It's written in Python.


[SentenceGenerator](https://github.com/ahplummer/SentenceGenerator) is a simple Sentence Generator that you can hook up to Slack and "roll dice" and get randomly constructed sentences. You can see it working [here](http://jutestring.com:5000/).

[Covid SMS](https://github.com/ahplummer/covidsms) is a Twilio integrated Go app that runs against the [NY Times Github](https://raw.githubusercontent.com/nytimes/covid-19-data/master/us-counties.csv). Read the readme over there how to use this.

[Slack-define](https://github.com/ahplummer/slack-define) is written in Python and is used in Slack to define words against an online dictionary, but also allows a team to add to a kind of localized 'urban dictionary' to collect up their own internal language.

[LeetSpeak](https://github.com/ahplummer/LeetSpeak) is written in Go, and converts an English sentence/word into LeetSpeak.

[Pirain](https://github.com/ahplummer/pirain) is written in Python, and designed to run on a Raspberry Pi, to use with [a relay expansion board](https://www.amazon.com/gp/product/B01G05KLIE/ref=oh_aui_search_asin_title?ie=UTF8&psc=1). This was before I bought my fabulous Rachio smart controller. It's designed to do a lookup of weather forecast for precipitation, then send a signal to the "moisture sensor" on the clock, thus overriding the regularly scheduled watering.  It basically turns your hooptie bottom-line Hunter/Rainbird controller into something a bit more "smart".  

[SiteMonitor](https://github.com/ahplummer/SiteMonitor) is a super simple webscraping monitor to make sure website is up. Written in Python, I had a need to quickly determine if a certain site was up, while working in an environment that was rather rigid in purchasing monitoring tools.